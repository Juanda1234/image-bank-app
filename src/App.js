import React, { useState, useEffect } from 'react';
import Formulario from './components/Formulario';
import ListadoImagenes from './components/ListadoImagenes';

function App() {

  const [ nameImagen, guardarNameImagen] = useState('');
  const [ imagenes, guardarImagenes ] = useState([]);
  const [ paginaactual, guardarPaginaActual ] = useState(1);
  const [ totalpaginas, guardarTotalPaginas ] = useState(1);

  
  useEffect(() => {

    if(nameImagen === "") return null;

    const consultarApi = async () => {
      
      const queryName = nameImagen.replace(/\s/g, "+");
      const imgsPaginas = 30;
      const api = '21067536-b955e0a3943a18ea384756c85';
      const url = `https://pixabay.com/api/?key=${api}&q=${queryName}&per_page=${imgsPaginas}&page=${paginaactual}&image_type=photo`;

      const callApi = await fetch(url);
      const respuesta = await callApi.json();
      console.log('respuesta de la api', respuesta);
      guardarImagenes(respuesta.hits);

      //calcular el total de paginas

      const calcularTotalPaginas = Math.ceil(respuesta.totalHits/imgsPaginas);

      guardarTotalPaginas(calcularTotalPaginas);

      //Mover la pantalla hacia arriba
      const jumbotron = document.getElementById('listaImagenes');
      jumbotron.scrollIntoView({ behavior: 'smooth' });



    }

    consultarApi();

  }, [nameImagen, paginaactual])

  //definir la pagina anterior

  const paginaAnterior = () => {
    const nuevaPaginaActual = paginaactual - 1;

    if(nuevaPaginaActual === 0) return;

    guardarPaginaActual(nuevaPaginaActual);

    console.log(nuevaPaginaActual);
  }

  const paginaSiguiente = () => {
    const nuevaPaginaActual = paginaactual + 1;

    if(nuevaPaginaActual > totalpaginas) return;

    guardarPaginaActual(nuevaPaginaActual);


  }

  return (
    <div className="container">
      <div className="jumbotron">
        <p className="lead text-center">Buscador de Imágenes</p>

        <Formulario
          guardarNameImagen={guardarNameImagen}
        />

      </div>
      <div id="listaImagenes" className="row justify-content-center">
        <ListadoImagenes
          imagenes={imagenes}
        />
        {(paginaactual === 1 || imagenes.length === 0) ? null : 
        <button
          type="button"
          className="btn btn-info mr-1 mb-4"
          onClick={paginaAnterior}
        >&laquo; Anterior</button>}
        {(paginaactual === totalpaginas || imagenes.length === 0) ? null :   
        <button
          type="button"
          className="btn btn-info mb-4"
          onClick={paginaSiguiente}
        >Siguiente &raquo;</button>}
      </div>
    </div>
  );
}

export default App;
