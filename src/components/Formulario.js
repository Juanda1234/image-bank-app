import React, { useState } from 'react';
import Error from './Error';
import PropTypes from 'prop-types';

const Formulario = ({ guardarNameImagen }) => {

    const [ termino, guardarTermino ] = useState('');
    const [ error, guardarError ] = useState(false);

    const selectedTerm = e => {
        e.preventDefault();
        if(termino.trim() === "") return guardarError(true); 
        
        guardarError(false);
        guardarNameImagen(termino);
    }

    return ( 
        <form
            onSubmit={selectedTerm}
        >
            <div className="row">
                <div className="form-group col-md-8">
                    <input
                        type="text"
                        className="form-control form-control-lg"
                        placeholder="Busca una imagen, ejemplo: Drones"
                        onChange={e => guardarTermino(e.target.value)}
                    />
                </div>
                <div className="form-group col-md-4">
                    <input
                        type="submit"
                        className="btn btn-lg btn-info btn-block"
                        value="Buscar"
                    />
                </div>
            </div>

            {error ? <Error mensaje="Agregar un término de busqueda"/> : null}
        </form>
     );
}

Formulario.propTypes = {
    guardarNameImagen: PropTypes.func.isRequired
}
 
export default Formulario;